package org.antlr.v4.codegen.target;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.antlr.v4.codegen.CodeGenerator;
import org.antlr.v4.codegen.Target;

public class CangjieTarget extends Target {
    private static final HashSet<String> reservedWords = new HashSet<>(Arrays.asList(
            "as", "abstract", "break",
            "Bool", "case", "catch",
            "class", "const", "continue",
            "Rune", "do", "else",
            "enum", "extend", "for",
            "func", "false", "finally",
            "foreign", "Float16", "Float32",
            "Float64", "if", "in",
            "is", "init", "import",
            "interface", "Int8", "Int16",
            "Int32", "Int64", "IntNative",
            "let", "mut", "main",
            "macro", "match", "Nothing",
            "open", "operator", "override",
            "prop", "public", "package",
            "private", "protected", "quote",
            "redef", "return", "spawn",
            "super", "static", "struct",
            "synchronized", "try", "this",
            "true", "type", "throw",
            "This", "unsafe", "Unit",
            "UInt8", "UInt16", "UInt32",
            "UInt64", "UIntNative", "var",
            "VArray", "where", "while"));

    protected CangjieTarget(CodeGenerator gen) {
        super(gen);
        // TODO Auto-generated constructor stub
    }

    @Override
    protected Set<String> getReservedWords() {
        return reservedWords;
    }

}
