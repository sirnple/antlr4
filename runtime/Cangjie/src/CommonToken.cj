package antlr4_runtime_cj

import antlr4_runtime_cj.atn.ATNSimulator
import antlr4_runtime_cj.misc.Interval
import antlr4_runtime_cj.misc.CharStream

public class CommonToken <: WritableToken {
    internal var `type`: Int
    internal var line = 0
    internal var charPositionInLine = -1
    internal var channel = Token.DEFAULT_CHANNEL
    internal let source: TokenSourceAndStream
    internal var text: ?String = None
    internal var index = -1
    internal var start = 0
    internal var stop = 0
    private var _visited = false

    public init(`type`: Int) {
        this.`type` = `type`
        this.source = TokenSourceAndStream.EMPTY
    }

    public init(source: TokenSourceAndStream, `type`: Int, channel: Int, start: Int, stop: Int) {
        this.source = source
        this.`type` = `type`
        this.channel = channel
        this.start = start
        this.stop = stop
        if (let Some(tsource) <- source.tokenSource) {
            this.line = tsource.getLine()
            this.charPositionInLine = tsource.getCharPositionInLine()
        }
    }

    public init(`type`: Int, text: ?String) {
        this.`type` = `type`
        this.channel = CommonToken.DEFAULT_CHANNEL
        this.text = text
        this.source = TokenSourceAndStream.EMPTY
    }

    public init(oldToken: Token) {
        this.`type` = oldToken.getType()
        this.line = oldToken.getLine()
        this.index = oldToken.getTokenIndex()
        this.charPositionInLine = oldToken.getCharPositionInLine()
        this.channel = oldToken.getChannel()
        this.start = oldToken.getStartIndex()
        this.stop = oldToken.getStopIndex()
        this.text = oldToken.getText()
        this.source = oldToken.getTokenSourceAndStream()
    }

    public func getType(): Int {
        return `type`
    }

    public func setLine(line: Int) {
        this.line = line
    }

    public func getText(): ?String {
        if (let Some(text) <- text) {
            return text
        }
        if (let Some(input) <- source.stream) {
            let n = input.size()
            if (start < n && stop < n) {
                input.getText(Interval.of(start, stop))
            } else {
                "<EOF>"
            }
        }
        return None
    }

    public func setText(text: String) {
        this.text = text
    }

    public func getLine(): Int {
        return line
    }

    public func getCharPositionInLine(): Int {
        return charPositionInLine
    }
    
    public func setCharPositionInLine(charPositionInLine: Int) {
        this.charPositionInLine = charPositionInLine
    }
    
    public func getChannel(): Int {
        return channel
    }
    
    public func setChannel(channel: Int) {
        this.channel = channel
    }
    
    public func setType(`type`: Int) {
        this.`type` = `type`
    }

    public func getStartIndex(): Int {
        return start
    }

    public func setStartIndex(start: Int) {
        this.start = start
    }
    
    public func getStopIndex(): Int {
        return stop
    }

    public func setStopIndex(stop: Int) {
        this.stop = stop
    }

    public func getTokenIndex(): Int {
        return index
    }

    public func setTokenIndex(index: Int) {
        this.index = index
    }

    public func getTokenSource(): ?TokenSource {
        return source.tokenSource
    }

    public func getInputStream(): ?CharStream {
        return source.stream
    }

    public func getTokenSourceAndStream(): TokenSourceAndStream {
        return source
    }

    public func toString(r: ?Recognizer<ATNSimulator>): String {
        let channelStr = if (channel > 0) { ",channel=${channel}" } else { "" }
        var txt: String
        if (let Some(tokenText) <- getText()) {
            txt = tokenText.replace("\n", "\\n")
            txt = txt.replace("\r", "\\r")
            txt = txt.replace("\t", "\\t")
        } else {
            txt = "<no text>"
        }

        let typeString: String
        if (let Some(r) <- r) {
            typeString = r.getVocabulary().getDisplayName(`type`)
        } else {
            typeString = "${`type`}"
        }
        return "[@${index},${start}:${stop}='${txt}':${typeString}${channelStr}]"
    }

    public func getRepresentative(): String {
        return toString(None)
    }

    public mut prop visited: Bool {
        get() { _visited }
        set(visited) { _visited = visited }
    }
}