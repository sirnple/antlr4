package antlr4_runtime_cj.misc
public struct Interval<: Hashable & Equatable<Interval> {
    public static let INVALID: Interval = Interval(-1, -2)
    public Interval(public var a: Int, public var b: Int) {}

    public static func of(a: Int, b: Int): Interval {
        return Interval(a, b)
    }

    public func length(): Int {
        if (b < a) {
            return 0
        } else {
            return b - a + 1
        }
    }

    public override func hashCode(): Int {
        var hash = 23
        hash = 31 * hash + a
        hash = 31 * hash + b
        return hash
    }

    public override operator func ==(other: Interval): Bool {
        return a == other.a && b == other.b
    }

    public override operator func !=(other: Interval): Bool {
        return !(this == other)
    }

    public func startsBeforeDisjoint(other: Interval): Bool {
        return a < other.a && b < other.a
    }

    public func startsBeforeNonDisjoint(other: Interval): Bool {
        return a <= other.a && b >= other.a
    }

    public func startsAfter(other: Interval): Bool {
        return a > other.a
    }
    
    public func startsAfterDisjoint(other: Interval): Bool {
        return a > other.b
    }
    
    public func startsAfterNonDisjoint(other: Interval): Bool {
        return a > other.a && a <= other.b
    }

    public func disjoint(other: Interval): Bool {
        return startsBeforeDisjoint(other) || startsAfterDisjoint(other)
    }

    public func adjacent(other: Interval): Bool {
        return a == other.b + 1 || b == other.a - 1
    }

    public func properlyContains(other: Interval): Bool {
        return other.a >= a && other.b <= b
    }

    public func union(other: Interval): Interval {
        return Interval(min(a, other.a), max(b, other.b))
    }

    public func intersection(other: Interval): Interval {
        return Interval(max(a, other.a), min(b, other.b))
    }

    public func differenceNotProperlyContained(other: Interval): ?Interval {
        var diff: ?Interval = None 
        if (other.startsBeforeNonDisjoint(this)) {
            diff = Interval(max(a, b + 1), b)
        } else if (other.startsAfterNonDisjoint(this)) {
            diff = Interval(a, a - 1)
        }
        return diff
    }

    public prop description: String {
        get() {
            "${a}..${b}"
        }
    }
}